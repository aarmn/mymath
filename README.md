# MyMath

Some of math functions and theories by me. Alireza Mohammadnezhad

    def xNumberDivisorsList(x,From=1,To=None,BoolFuncOnlyDivisors=None):
	'''
	x : number we want to find its divisors (number,natural)
	BoolFuncOnlyDivisors : the Bool function we want to filter our outputs with e.g. MersennePrimeBool() (Function, number input, bool output)
	From : the first value to check (number,natural)
	To : the last value to check (number,natural)
	this function stop when find all of number divisors in your defined range 
	return list (of divisors)
	'''
	if not (NaturalBool(x) and NaturalBool(From) and NaturalBool(To)):
		raise ValueError("x,From,To should be Natural numbers")
	counter=From
	if (To!=None):
		if(To>x):
			warnings.warn("It's reasonless to try find natural divisor bigger than the given number!")
		max=To
	else:
		max=x
	list=[]
	if(BoolFuncOnlyDivisors==None):
		while(counter<=max):
			if(x%counter==0):
				list.append(counter)
			counter+=1
	else:
		while(counter<=max):
			if(x%counter==0):
				if(BoolFuncOnlyDivisors(counter)):
					list.append(counter)
			counter+=1
	return list

    def DiffFromNum(x):
        return (x-sum(xNumberDivisorsList(x,To=(x-1))))

    def DiffFromNumRangeRev(x):
        Ans={0:[1]}
        for i in range(2,x+1):
            tmp=DiffFromNum(i)
            if (Ans.get(tmp)==None):
                Ans.update({tmp:[i]})
            else:
                Ans[tmp]+=[i]
        return Ans

    def DiffFromNumSum(x,EvolveList=False):
        sum=0
        Evolve=[]
        for i in range (2,x):
            sum+=DiffFromNum(i)
            if (EvolveList):
                Evolve.append(sum)
        if (EvolveList):
            return Evolve
        return sum
    # From maryam project by me

 ### Diff From Num is an operation that you minus number from all of its divisors but that exact number, I thing it's a cool operation and if I find any good point about it i will update here 
 - some fact for example:
 - all primes diff from num is equal to (thatprime-1)
 - all 2**x numbers diff from num is 1
 - all perfect numbers diff from num is 0
 - there are more positive diff from num up to the range i tested it. but i don't have a mathematical proof for it and i think maybe it reverse 

if you found any fact about this report me to work on it
